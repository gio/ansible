#!/usr/bin/python3

import time
import curses

import serial


def input1_on(opsis):
    opsis_command(opsis, 'input1 on\r')


def connect_output1(opsis, input_):
    opsis_command(opsis, 'video_matrix connect {} output1\r'.format(input_))


def input1_status(opsis):
    status = opsis_command(opsis, 'status\r')

    filtered = [line for line in status.splitlines()
                if line.startswith('input1:')]
    return filtered[0]


def main():
    opsis = opsis_open()
    win = curses.initscr()
    win.clear()
    curses.noecho()

    height, width = win.getmaxyx()
    title = 'Speaker Test Station'
    win.addstr(0, 0, '#' * width)
    win.addstr(1, 0, '##')
    win.addstr(1, (width - len(title)) // 2, title)
    win.addstr(1, width - 2, '##')
    win.addstr(2, 0, '#' * width)

    win.addstr( 5, 0, 'Instructions:')
    win.addstr( 6, 0, '1. Plug your laptop into the loose HDMI cable.')
    win.addstr( 7, 0, '2. Press any key on this machine, if nothing happens.')
    win.addstr( 8, 0, '3. Your machine should see an extra monitor.')
    win.addstr( 9, 0, '4. Test your presentation. '
                      'Make sure that it looks OK.')
    win.addstr(10, 0, '5. Unplug the HDMI cable, and enjoy the conference.')

    win.addstr(12, 0, 'If you run into any problems, please get hold of the '
                      'video team,')
    win.addstr(13, 0, "they're only too happy to help.")

    win.refresh()
    win.timeout(1)
    output1 = 'unknown'
    try:
        while True:
            keypress = win.getch()

            if keypress != -1:
                input1_on(opsis)
                win.addstr(21, 0, 'input1 reset')
                win.refresh()

            input1 = input1_status(opsis)
            if ' 0x0 ' in input1:
                if output1 != 'pattern':
                    connect_output1(opsis, 'pattern')
                    output1 = 'pattern'
            else:
                if output1 != 'input1':
                    connect_output1(opsis, 'input1')
                    output1 = 'input1'

            win.addstr(20, 0, 'We see: ' + input1)
            win.clrtobot()
            win.refresh()
    finally:
        curses.endwin()


def opsis_command(opsis, string):
    for character in string:
        opsis.write(character.encode('utf-8'))
        time.sleep(0.01)
    opsis.flush()
    time.sleep(0.25)
    return opsis.read(1024).decode('utf-8')


def opsis_open():
    """Open the serial port to the opsis"""
    return serial.Serial('/dev/hdmi2usb/by-num/all0/tty0', 115200, timeout=1)


if __name__ == '__main__':
    main()
