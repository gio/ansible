# test-station

Depends on the `opsis` role.

A speaker test-station, so that speakers can test their laptops with the
Opsis.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `user_name`:             Main user username.
