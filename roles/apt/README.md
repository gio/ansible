# apt

Manage and configure apt.

## Tasks

The tasks are divided into:

* `sources.yml`:        Managing `/etc/apt/sources.list`
* `sources.d.yml`:      Managing extra repos in `/etc/apt/sources.d`
* `config.yml`:         Managing APT configuration
* `local_repo.yml`:     Managing a `local-apt-repository`, containing
                        any local debs needed.

## Available variables

Main variables are:

* `debian_source`:      The Debian source you want to use.

* `debian_version`:     The Debian codename you want to use.

* `apt_proxy`:          Boolean variable. If set to false, it will remove the
                        apt proxy D-I set one.

* `update_apt_sources`: Boolean value. If set to false, this role won't touch
                        your apt source.list file.

* `enable_sid`:         Boolean value. Enable sid in the apt sources.

* `enable_oldstable`:   Boolean value. Enable oldstable in the apt sources.

* `extra_debs`:         List of URLs to `.deb` files to make available to APT.
